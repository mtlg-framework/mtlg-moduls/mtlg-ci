Staging server for games made with the mtlg gameframework. The server is run by node and a docker wrapper is provided.

To run the server as docker image with configuration

- serve on port `80`
- store games in `/home/games`
- require token `abcd1234` for game upload

, following commands can be used:

```
git clone git@gitlab.com:learntech-rwth/mtlg-framework/mtlg-ci.git
docker build -t tabula mtlg-ci
docker run -d -p 80:8080 -v /home/games:/home/public -e DEPLOY_TOKEN=abcd1234 tabula
```

### preview.json

To customize a game's preview, it must contain the file `manifest/preview.json` in the root. The content is as follows:

```
{
  "name": "My Game",
  "shorttext": "A game you definitely will love!",
  "thumbnail": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAACACAYAAADktbcKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gIBFzA6VQd+mAAAAXhJREFUeNrt3UEOhCAQRUHbcP8rf0/gAhIhSNV6NsPipTVNrCS5gCPdjgAEABAAQAAAAQAEABAAQAAAAQAEANhYcwSsVlVdv7e9bgIABAAQAMA7gFnPoLs+h57yPzEBAAIACAAgAIAAAAIACAAIACAAgAAAZ7AKzHLWjU0AgAAAAgAIACAAgAAAAgAIACAAgAAAAgB0cReA5XwazAQACAAgAIAAAAIACAAgAIAAAAIACAAgAIAAAAIACADwxnXgQb1XWMEEAAgAIACAAAACAAgAIACAAAACAHzCJuCgHT9OYXsREwAgAIAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgACAAAACAAgAIADA31WSOAYwAQACAAgAIACAAAACAAgAIACAAAACAAgAIACAAAACAAgAIACAAAACAAgAMN0D7PUbA+1sVS8AAAAASUVORK5CYII="
}
```

The value of `shorttext` should be at most 80 characters long. The value of `thumbnail` must be an image-data-url, the image should be of dimenstion 256x128.

#### develop

To start development, use following commands:

```
# download this repo from the internet
git clone git@gitlab.com:mtlg-framework/mtlg-moduls/mtlg-ci.git
cd mtlg-ci

# install the mtlg-framework
npm install

# build the frontend
mtlg build

# create an empty games-folder
mkdir public

# start the server
node ./server.js 127.0.0.1 1234

# open your browser at 127.0.0.1:1234?showall
```

To add games, populate the folder `public/` in the following way:

```
public
  ├─ game1
  │   ├─ index.html
  │   …
  ├─ game2
  …   ├─ index.html
      …
```

When you make changes to the frontend, i.e. when you modify files in the `dev/` folder, use following command to rebuild:

```
mtlg build
```

To test a game upload:

```
export DEPLOY_TOKEN=abcd1234 && node ./server.js 127.0.0.1 1234
curl --data-binary @game.tar.gz 127.0.0.1:1234/best-game-ever?token=abcd1234
```

To simulate the docker-container environment, create the folder `/home/games` and link it to `public/`:

```
ln -s /home/games public
```

To test the docker container, run:

```
docker build -t tabula .
docker run -d -p 1234:8080 -v /home/games:/home/public -e DEPLOY_TOKEN=abcd1234 tabula
# open browser at localhost:1234
```
