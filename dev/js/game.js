
// expose framework for debugging
window.MTLG = MTLG;
// framework must be started
document.addEventListener("DOMContentLoaded", MTLG.init);

require('../manifest/device.config.js');
require('../manifest/game.config.js');

MTLG.lang.define({'en': {}});

MTLG.addGameInit(() => {
  var games = null;

  // game size: 1920 x 1080

  MTLG.lc.registerMenu(() => {
    var stage = MTLG.getStageContainer();

    // blue background
    var back = new createjs.Shape();
    back.graphics.rf(["rgba(0,55,185,1)", "rgba(130,150,245,1)"], [0,1], 800,500,400, 800,540,1000).r(0,0,1920,1080);
    stage.addChild(back)

    if(games == null) {
      stage.addChild(new createjs.Text("☕", "300px Arial").set({x:960,y:540,textAlign:"center",textBaseline:"center",color:"#ff8800"}));
    } else if(games.length == 0) {
      stage.addChild(new createjs.Text("No games uploaded yet.", "100px Arial").set({x:960,y:540,textAlign:"center",textBaseline:"center",color:"#ff8800"}));
    } else {
      // gamecard layout:
      //                 fill up 3x4 pattern                  │              keep 3 rows
      // n = 1 2  3   4    5    6    7    8    9    10   11   12   13    14    15    16     17     18     19
      //     •│••│•••│••••│••••│••••│••••│••••│••••│••••│••••│••••│•••••│•••••│•••••│••••••│••••••│••••••│•••••••│ …
      //      │  │   │    │•   │••  │••• │••••│••••│••••│••••│••••│•••••│•••••│•••••│••••••│••••••│••••••│•••••••│ …
      //      │  │   │    │    │    │    │    │•   │••  │••• │••••│•••  │•••• │•••••│••••  │••••• │••••••│•••••  │ …
      var W = 400, H = 300, n = games.length, nCols = Math.max(4,Math.ceil(n/3)), nRows = Math.min(3,Math.ceil(n/4));
      var areas = [...Array(n)].map((_,i) => (new createjs.Container()).set({
        x: (i % nCols) * W,
        y: Math.floor(i/nCols) * H
      }));

      // scrollpane, also used for centering the gamecards
      var pane = new createjs.Container().set({x:160, y:540-.5*nRows*H});
      // hitplane makes scrollpane scrollpane scrollable on every point
      var hitplane = new createjs.Shape();
      hitplane.graphics.f("rgba(0,0,0,.01)").r(-500,-500,5000,2080);
      let x,y;
      var pan;
      stage.addChild(pane);
      if(n > 12) {
        pane.on("mousedown", e => [pan,x] = [false,e.localX]);
        pane.on("pressmove", e => {pan=true; pane.set({x:Math.min(160,Math.max(1920-(160+nCols*W), pane.x-x+e.localX))})});
      }
      pane.addChild(hitplane, ...areas);

      // gamecards
      for(var i in games) {
        // yellow-green gamecard background
        var frame = new createjs.Shape();
        frame.graphics.lf(["rgba(0,125,255,0)","rgba(190,255,200,.5)","rgba(190,255,200,1)","rgba(230,255,150,1)","rgba(230,255,150,.5)","rgba(0,125,255,0)"],
          [0,.1,.4,.6,.9,1], 0,0, W,0).r(10,10,W-20,H-20);

        // game name, falls back to folder name
        var name = new createjs.Text(games[i].name || games[i].repo, "60px Arial").set({x:.5*W,y:10,maxWidth:300,color:"#000055",textAlign:"center",skewX:5});
        name.shadow = new createjs.Shadow("#aaaacc", 2, 2, 5);

        // short description
        var shorttext = new createjs.Text(games[i].shorttext || "", "23px Arial").set({color:"#000010",x:50,y:75,maxWidth:300,lineWidth:350});
        // cut outstanding text
        shorttext.cache(0,0,300,70);

        // game thumbnail, falls back to brush image
        var image = games[i].thumbnail ? new createjs.Bitmap(games[i].thumbnail) : MTLG.assets.getBitmap("img/brush.svg");
        image.set({x:.5*(W-256),y:155});

        // download button
        let download = MTLG.assets.getBitmap("img/download.svg").set({x:W-80,y:H-70});
        download.cache(0,0,50,50);
        download.on("mousedown", () => download.set({filters: [new createjs.ColorMatrixFilter([.3,0,0,0,50, .3,0,0,0,50, 1,0,0,0,50, 0,0,0,1,0])]}).updateCache());
        download.on("pressup", () => {
          download.set({filters: []}).updateCache();
          // start download
          window.location = games[_i].downloadUrl;
        });

        // follow game link on gamecard click
        var clickpane = new createjs.Container();
        clickpane.addChild(frame, name, shorttext, image);
        let _i = i;
        clickpane.on("click", e => {
          if(!pan) {
            // animation while establishing network connection
            areas[_i].set(pane.localToLocal(areas[_i].x, areas[_i].y, stage));
            stage.addChild(areas[_i]);
            createjs.Tween.get(areas[_i]).to({x:0,y:0,scaleX:1920/W,scaleY:1080/H,},2000);
            window.location = games[_i].url;
          }
        })

        areas[i].addChild(clickpane, download);
      }
    }
  }, null, ["img/brush.svg","img/download.svg"]);
  fetch('games').then(res => res.json()).then(_games => {
    games = _games;
    if(window.location.search != "?showall")
      games = games.filter(g => g.name != null);
    MTLG.lc.goToMenu();
  });
})
