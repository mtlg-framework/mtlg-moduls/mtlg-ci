const ProvidePlugin = require('webpack').ProvidePlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const framework_root = require.resolve('mtlg-gameframe/package.json').match(/.*(?=\/package.json$)/)[0];

// needed for `webpack --watch`
const imageassets = {}, soundassets = {};

module.exports = {
  mode: 'development',
  // bundle javascript
  entry: './dev/js/game.js',
  output: {
    path: `${__dirname}/build`,
    filename: 'game.js'
  },
  resolve: { symlinks: false},
  module: { rules: [
    // bundle css
    { test: /\.css$/, loader: 'style-loader!css-loader' } ,
    // bundle framework and module assets (images and sounds)
    { test: new RegExp(`^${__dirname}/.*/(img|sounds)/`),
      use: [ {
        loader: 'file-loader',
        options: { name: path => path.match(new RegExp(`^${__dirname}/.*/((img|sounds)/.*)$`))[1] }
      } ] }
  ] },
  plugins: [
    new ProvidePlugin({
      MTLG: 'mtlg-core',
      createjs: 'exports-loader?createjs!createjs/builds/1.0.0/createjs.min.js',
      // TODO: workaround, remove window.createjs alias as soon as https://github.com/CreateJS/PreloadJS/issues/150 gets fixed
      'window.createjs': 'exports-loader?createjs!createjs/builds/1.0.0/createjs.min.js'
    }),
    new CopyWebpackPlugin([
      // bundle manifest, game assets, index.html and html-dependencies
      {from: './dev/manifest', to: './manifest'},
      {from: './dev/img', to: './img'},
      {from: './dev/sounds', to: './sounds'},
      {from: `${framework_root}/build_essentials/index.html`, to: '.'},
      // external dependencies should be replaced with npm-requires
      {from: `${framework_root}/build_essentials/extern`, to: './lib'}
    ]),
    // create image and sound manifests for preloading
    new ManifestPlugin({
        fileName: './imageassets.json',
        seed: {},
        generate: (seed, files) => { files.forEach(({name, path}) => seed[name]=path); return Object.values(seed); },
        filter: f => /^img\//.test(f.path)
    }),
    new ManifestPlugin({
        fileName: './soundassets.json',
        seed: {},
        generate: (seed, files) => { files.forEach(({name, path}) => seed[name]=path); return Object.values(seed); },
        filter: f => /^sounds\//.test(f.path)
    })
  ]
};
