FROM node:8
RUN apt-get update && apt-get install -y zip
WORKDIR /home
COPY ./package.json .
RUN npm install
COPY ./dev /home/dev
RUN ./node_modules/.bin/mtlg build production
COPY ./server.js .
RUN mkdir archives
VOLUME /home/public
ENTRYPOINT ["/usr/bin/env", "node", "./server.js"]
