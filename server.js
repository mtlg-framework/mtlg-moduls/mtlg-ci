#!/usr/bin/env node

http = require('http')
path = require('path')
url = require('url')
util = require('util')
fs = require('fs')
net = require('net')
os = require('os')
child_process = require('child_process')


// input validation

help = () => console.log(`
usage:
\x1b[1m./server.js\x1b[0m [host [port]]   starts server, defaults to eth0:8080
                            serves games from folder ./public
`)
var host = process.argv[2] || (os.networkInterfaces()["eth0"] ? os.networkInterfaces()["eth0"][0].address : null)
var port = process.argv[3] || 8080
if(!net.isIP(host))
  return help()
if(isNaN(port))
  return help()


// static file routing

get_file_path = (url) => {
  // transform file path
  return new Promise((ful,rej) => {
    // file outside webroot
    if(!path.resolve(`.${url}`).startsWith(path.resolve('.')))
      rej("file outside webroot")
    else {
      // route to frontend or game
      if(/^\/games\//.test(url))
        url = "/public" + /^\/games(.*)/.exec(url)[1]
      else
        url = "/build" + url
      fs.stat(`.${url}`, (err, stats) => {
        if(err) rej(err)
        else if(stats.isFile())
          // normal file
          ful([`.${url}`, stats.size])
        else if(stats.isDirectory())
          fs.stat(`.${url}/index.html`, (err, stats) => {
            if(err || !stats.isFile()) rej(err || "url is not a file")
            // index.html
            else ful([`.${url}/index.html`, stats.size])
          })
      })
    }
  })
}


// extract zip to webroot (depends on 'tar'-program)

unzip = (name, zip_stream) => {
  return new Promise((ful, rej) => {
    // write zip to disk
    zip_out = fs.createWriteStream(`./archives/${name}.tar.gz`, {encoding: 'binary'})
    zip_stream.setEncoding('binary')
    zip_stream.pipe(zip_out)
    zip_out.on('finish', () => {
      // replace old version with new version, delete old archives
      cp = child_process.exec(
        `(rm -rf ./public/${name} ./archives/${name}.zip && mkdir ./public/${name} && tar -xf ./archives/${name}.tar.gz -C ./public/${name}) 2>&1`
        , (err, stdout, stderr) => {
            if(err) {console.error(`${err}\n${stdout}`); rej('upload failed due to a server error, sorry!')}
            else     ful()
      })
    })
  })
}


// get gamezip (depends on 'tar' and 'zip'-program)

getzip = (name, ext) => {
  return new Promise((ful,rej) => {
    var path = `./archives/${name}.${ext}`, size
    // check archive exists
    fs.stat(path, (err, stats) => {
      if(stats)                             ful([path, stats.size])
      else if(err && err.code != "ENOENT")  rej([500, `failed to serve game file`, err])
      else if(err && err.code == "ENOENT")  {
        // archive doesn't exist, check game exists
        fs.stat(`./public/${name}`, (err, stats) => {
          if(err && err.code == "ENOENT")  rej([404, `game doesn't exist`])
          else if(err)                     rej([500, `failed to serve game file`, err])
          // zip game
          else cp = child_process.exec({
            'tar.gz': `tar -czf ${path} -C ./public ${name} 2>&1`,
            'zip':    `(cd ./public && zip -r ${name}.zip ./${name}) && mv ./public/${name}.zip ${path}`
          }[ext], (err, stdout, stderr) => {
            if(err)  rej([500, `failed to serve game file`, err])
            else     fs.stat(path, (err, stats) => {
                        if(stats)  ful([path, stats.size])
                        else       rej([500, `failed to serve game file`, err])
}) }) }) } }) }) }

// http server

server = http.createServer((req, res) => {
  var {pathname, query} = url.parse(req.url, true)
  if(req.method == 'POST') {
    // upload game
    // try out with e.g.
    // curl --data-binary @game.tar.gz 127.0.0.1:1235
    name = pathname.slice(1)
    if(!/^[0-9a-zA-Z_-]+$/.test(name)) {
      res.writeHead(400, {'Content-Type': 'text/plain; charset=utf-8'})
      res.end(`bad game name: [${name}], must match [0-9a-zA-Z_-]+`)
    } else if(query.token != process.env['DEPLOY_TOKEN']) {
      res.writeHead(403, {'Content-Type': 'text/html; charset=utf-8'})
      res.end(`a valid token must be used for deployment`)
    } else {
      unzip(name, req)
      .then((htmlpage) => {
        res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'})
        res.end(`game successfully uploaded`)
      }).catch((e) => {
        res.writeHead(500, {'Content-Type': 'text/html; charset=utf-8'})
        res.end(`Error during upload: ${e}`)
      })
    }
  } else if(pathname == '/games') {
    // serve game overview
    var games
    util.promisify(fs.readdir)('./public')
    .then(files => {
      games = files
      // load all previews
      return Promise.all(games.map(g => new Promise((ful,rej) => {
        var result = {repo:g,url:`games/${g}/`,downloadUrl:`download/${g}.zip`}
        util.promisify(fs.stat)(`./public/${g}/manifest/preview.json`)
        .then(_ => util.promisify(fs.readFile)(`./public/${g}/manifest/preview.json`))
        .then(content => ful(Object.assign(result, JSON.parse(content))))
        .catch(err => ful(result))
      })))
    }).then(previews => {
      res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'})
      res.end(JSON.stringify(previews))
    }).catch((e) => {
      console.error(e)
      res.writeHead(500, {'Content-Type': 'text/html; charset=utf-8'})
      res.end(`failed to acquire game list`)
    })
  } else if(pathname.startsWith('/download/')) {
    // download game
    var match = /\/download\/([0-9a-zA-Z_-]+)\.(tar\.gz|zip)/.exec(pathname)
    if(match) {
      getzip(match[1], match[2])
      .then(([localpath, filesize]) => {
        res.writeHead(200, {
          'Content-Type': {'tar.gz':`application/gzip`, 'zip':`application/zip`}[match[2]],
          'Content-Length': filesize,
          'Content-Disposition': `attachment; filename=${match[1]}.${match[2]}`
        })
        s = fs.createReadStream(localpath)
        s.pipe(res)
      }).catch(([httpCode, msg, err]) => {
        console.error(err)
        res.writeHead(httpCode, {'Content-Type': 'text/plain; charset=utf-8'})
        res.end(msg)
      })
    } else {
      res.writeHead(400, {'Content-Type': 'text/plain; charset=utf-8'})
      res.end(`bad download path: [${pathname}], must match /download/[0-9a-zA-Z_-]+.(tar.gz|zip)`)
    }
  } else {
    // serve static file
    let localpath
    get_file_path(pathname)
    .then(([localpath, filesize]) => {
      contenttypes = {'.html':'text/html', '.js':'application/javascript', '.css':'text/css', '.styl':'text/css', '.svg':'image/svg+xml', '.png':'image/png', '.jpg':'image/jpg', '.webm':'video/webm'}
      res.writeHead(200, {
        'Content-Type': `${contenttypes[path.extname(localpath)]}; charset=utf-8`,
        'Content-Length': filesize
      })
      s = fs.createReadStream(localpath)
      s.pipe(res)
    }).catch((e) => {
      res.writeHead(404, {'Content-Type': 'text/html; charset=utf-8'})
      res.end(`Path <em>${pathname}</em> not accessible on server`)
    })
  }
})
server.listen(port, host, ()=>console.log(`Server running at http://${host}:${port}/`))
